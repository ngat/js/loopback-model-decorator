import { ModelRegister } from './register';

export function Model(arg?: { hooks?: {}, remotes?: {} }) {
    function f(target: any) {
        function ff(reference: any) {
            let instance = new target(reference);
            if (!arg || Object.keys(arg).length === 0) {
                return instance;
            }
            instance = (<any>Object).assign(instance, arg);
            new ModelRegister(instance, reference);
            return instance;
        }
        return <any>ff;
    }
    return f;
}
