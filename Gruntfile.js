'use strict';
require('babel-register')({
});

module.exports = grunt => {
  require('time-grunt')(grunt);
  grunt.initConfig({
    exec: {
      tsc: {
        command: 'tsc -d -p .'
      }
    }
  });
  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('default', ['exec:tsc']);

};
