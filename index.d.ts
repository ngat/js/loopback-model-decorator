export declare function Model(arg?: {
    hooks?: {};
    remotes?: {};
}): (target: any) => any;
