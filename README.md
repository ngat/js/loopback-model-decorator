[![pipeline status](https://gitlab.com/ngat/js/loopback-model-decorator/badges/master/pipeline.svg)](https://gitlab.com/ngat/js/loopback-model-decorator/commits/master)

# Loopback Model Decorator

Loopback Model Decorator provides a typescript decorator for creating and configuring a Loopback Model

Based on [@mean-expert/model](https://www.npmjs.com/package/@mean-expert/model) and [@mean-expert/model-register](https://www.npmjs.com/package/@mean-expert/model-register) which lacked support for events

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
npm install -g loopback-cli
mkdir example
cd example
lb
npm install @types/loopback -S
```

### Installing

Install Model Decorator Module via npm

```
npm install loopback-model-decorator -S
```

Install Model Decorator Module via yarn

```
yarn add loopback-model-decorator
```

Create loopback model manually or via lb command

```
lb model
```

Modify model JavaScript file to use Loopback Model Descriptor

```typescript
import {LoopBackApplication, PersistedModel} from "loopback";
import {Model} from "loopback-model-decorator";

@Model({
  hooks: {
    attached: {name: 'attached', type: 'event'},
    access: {name: 'access', type: 'operation'},
    persist: {name: 'persist', type: 'operation'},
    afterSave: {name: 'after save', type: 'operation'},
    beforeSave: {name: 'before save', type: 'operation'},
    beforeDelete: {name: 'before delete', type: 'operation'},
    afterDelete: {name: 'after delete', type: 'operation'},
    beforeMyRemote: {name: 'myRemote', type: 'beforeRemote'},
    afterMyRemote: {name: 'myRemote', type: 'afterRemote'},
  },
  remotes: {
    myRemote: {
      returns: {arg: 'result', type: 'array'},
      http: {path: '/my-remote', verb: 'get'}
    }
  }
})
export default class Example extends PersistedModel {
  app: LoopBackApplication;
  constructor(public model: any) {
    super(model);
  }
  attached(application: LoopBackApplication): void {
    let me = this;
    me.app = application;
  }
  
  access(ctx: any, next: Function): void {
    console.log('example: access');
    next();
  }

  persist(ctx: any, next: Function): void {
    console.log('example: persist');
    next();
  }

  beforeSave(ctx: any, next: Function): void {
    console.log('example: before Save');
    next();
  }

  beforeMyRemote(ctx: any, next: Function) {
    console.log('example: before myRemote');
    next();
  }

  myRemote(next: Function): void {
    console.log('example: myRemote');
    this.model.find(next);
  }

  afterMyRemote(ctx: any, next: Function) {
    console.log('example: after myRemote');
    next();
  }

  beforeDelete(ctx: any, next: Function): void {
    console.log('example: before Delete');
    next();
  }

  afterDelete(ctx: any, next: Function): void {
    console.log('example: after Delete');
    next();
  }
}
```

## Built With

* [@types/node](https://www.npmjs.com/package/@types/node)

## Versioning

We use [SemVer](http://semver.org/) for versioning.
For the versions available, see the [tags on this repository](https://gitlab.com/ngat/js/loopback-model-decorator/tags). 

## Authors

* **Colton McInroy** - *Initial work* - [GitLab Profile](https://gitlab.com/nextgenagritech)

See also the list of [contributors](https://gitlab.com/ngat/js/loopback-model-decorator/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [@mean-expert/model](https://www.npmjs.com/package/@mean-expert/model)
* [@mean-expert/model-register](https://www.npmjs.com/package/@mean-expert/model-register)
